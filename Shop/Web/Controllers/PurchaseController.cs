﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class PurchaseController : Controller
    {
        private ShopDb_Hassanuzzaman_01735807216Entities db = new ShopDb_Hassanuzzaman_01735807216Entities();
        public ActionResult Index()
        {
            return View(db.Purchase.Include("PurchaseItems").ToList());
        }

        public ActionResult Create()
        {
            ViewBag.Date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.Product = new SelectList(db.Product.ToList(), "Id", "Name");
            return View();
        }
        [HttpPost]
        public ActionResult Create(string SupplierPhone, string SupplierName, decimal Discount, DateTime Date, List<int> productId, List<decimal> productPrice, List<decimal> productTotalPrice, List<int> productQty, decimal TotalBill, decimal NetReceivable, decimal Payment, decimal Change)
        {

            try
            {
                var SupplierData = db.Supplier.Where(x => x.Phone == SupplierPhone).FirstOrDefault();
                if (SupplierData == null)
                {
                    Supplier person = new Supplier();
                    person.Name = SupplierName;
                    person.Phone = SupplierPhone;
                    db.Supplier.Add(person);
                    db.SaveChanges();
                    SupplierData = person;
                }
                decimal paidAmount = Payment - Change;
                var dt = new DataTable();
                dt.Columns.Add("ProductId");
                dt.Columns.Add("QTY");
                dt.Columns.Add("Price");


                // collect order items.... 
                for (int j = 0; j < productId.Count(); j++)
                {
                    var PriceRate = productPrice[j];
                    var Product_Id = productId[j];
                    int QTY = productQty[j];
                    dt.Rows.Add(Product_Id, QTY, PriceRate);
                }

                //save product order
                SqlParameter _date = new SqlParameter("@date", Date);
                SqlParameter _supplierId = new SqlParameter("@supplierId", SupplierData.Id);
                SqlParameter _totalAmount = new SqlParameter("@totalAmount", TotalBill);
                SqlParameter _discount = new SqlParameter("@discount", Discount);
                SqlParameter _paidAmount = new SqlParameter("@paidAmount", paidAmount);
                SqlParameter _itemList = new SqlParameter("@itemList", SqlDbType.Structured);
                _itemList.Value = dt;
                _itemList.TypeName = "PURCHASEITEMTYPE";
                db.Database.ExecuteSqlCommand("spSavePurchase @date,@SupplierId,@totalAmount,@discount,@paidAmount,@itemList", _date, _supplierId, _totalAmount, _discount, _paidAmount, _itemList);

            }
            catch (Exception ex)
            {
                return View("Create");
            }



            return RedirectToAction("Index");
        }
    }
}