﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class SalesController : Controller
    {
        private ShopDb_Hassanuzzaman_01735807216Entities db = new ShopDb_Hassanuzzaman_01735807216Entities();
        public ActionResult Index()
        {
            return View(db.Sales.Include("SalesItems").ToList());
        }

        public ActionResult Create()
        {
            ViewBag.Date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.Product = new SelectList(db.Product.ToList(), "Id", "Name");
            return View();
        }
        [HttpPost]
        public ActionResult Create(string CustomerPhone, string CustomerName, decimal Discount, DateTime Date, List<int> productId,  List<decimal> productPrice, List<decimal> productTotalPrice, List<int> productQty, decimal TotalBill, decimal NetReceivable, decimal Payment, decimal Change)
        {

            try
            {
                var customerData = db.Customer.Where(x => x.Phone == CustomerPhone).FirstOrDefault();
                if (customerData == null)
                {
                    Customer person = new Customer();
                    person.Name = CustomerName;
                    person.Phone = CustomerPhone;
                    db.Customer.Add(person);
                    db.SaveChanges();
                    customerData = person;
                }
                decimal paidAmount = Payment - Change;
                var dt = new DataTable();
                dt.Columns.Add("ProductId");
                dt.Columns.Add("QTY");
                dt.Columns.Add("Price");

                
                // collect order items.... 
                for (int j = 0; j < productId.Count(); j++)
                {
                    var PriceRate = productPrice[j];
                    var Product_Id = productId[j];
                    int QTY = productQty[j];
                    dt.Rows.Add(Product_Id,QTY,PriceRate);
                }

                //save product order
                SqlParameter _date = new SqlParameter("@date", Date);
                SqlParameter _customerId = new SqlParameter("@customerId", customerData.Id);
                SqlParameter _totalAmount = new SqlParameter("@totalAmount", TotalBill);
                SqlParameter _discount = new SqlParameter("@discount", Discount);
                SqlParameter _paidAmount = new SqlParameter("@paidAmount", paidAmount);
                SqlParameter _itemList = new SqlParameter("@itemList",SqlDbType.Structured);
                _itemList.Value = dt;
                _itemList.TypeName = "SALESITEMTYPE";
                db.Database.ExecuteSqlCommand("spSaveSales @date,@customerId,@totalAmount,@discount,@paidAmount,@itemList", _date, _customerId, _totalAmount, _discount, _paidAmount,_itemList);

            }
            catch (Exception ex)
            {
                return View("Create");
            }



            return RedirectToAction("Index");
        }



        public ActionResult Return()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Return(int Id)
        {
            var data = db.Sales.FirstOrDefault(x => x.Id == Id);
            if(data!=null)
            {
                return RedirectToAction("Details", new { id=Id});
            }
            ModelState.AddModelError("", "No Record Found !");
            return View();
        }

        public ActionResult Details(int id)
        {
            var data = db.Sales.Include("SalesItems").FirstOrDefault(x => x.Id == id);
            return View(data);
        }

        public ActionResult ItemDelete(int id, int salesId)
        {
            var data = db.Sales.Include("SalesItems").FirstOrDefault(x => x.Id == salesId);
            var item = data.SalesItems.FirstOrDefault(x => x.Id == id);
            db.SalesItems.Remove(item);
            db.SaveChanges();
            if (data.SalesItems.Count ==0)
            {
                db.Sales.Remove(data);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Details", new { id = id });
        }


    }
}