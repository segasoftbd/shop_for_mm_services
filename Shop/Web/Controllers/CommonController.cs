﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class CommonController : Controller
    {
        private ShopDb_Hassanuzzaman_01735807216Entities db = new ShopDb_Hassanuzzaman_01735807216Entities();

        public JsonResult getPhonenumber(string term)
        {


            var phonenumber = db.Customer.Where(x => x.Phone.StartsWith(term)).Select(s => s.Phone).Take(5).ToList();

            return Json(phonenumber, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPhonenumberSupplier(string term)
        {


            var phonenumber = db.Supplier.Where(x => x.Phone.StartsWith(term)).Select(s => s.Phone).Take(5).ToList();

            return Json(phonenumber, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductSalesPrice(int id)
        {
            var data = db.Product.Where(x => x.Id == id).Select(y => y.SalesPrice).FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductPurchasePrice(int id)
        {
            var data = db.Product.Where(x => x.Id == id).Select(y => y.PurchasePrice).FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult GetCustomer(string id)
        {

            var data = db.Customer.Where(x => x.Phone == id).Select(y => new {  Name = y.Name }).FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSupplier(string id)
        {

            var data = db.Supplier.Where(x => x.Phone == id).Select(y => new { Name = y.Name }).FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCustomerOrder(string id)
        {
            var customer = db.Customer.FirstOrDefault(x => x.Phone == id);
            List<SelectListItem> OrderList = new List<SelectListItem>();
            if (customer != null)
            {
                var order = db.Sales.Where(x => x.CustomerId == customer.Id).ToList();
                foreach (var item in order)
                {
                    OrderList.Add(new SelectListItem { Text = item.Id.ToString(), Value = item.Id.ToString() });
                }
            }

            return Json(OrderList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSupplierOrder(string id)
        {
            var supplier = db.Supplier.FirstOrDefault(x => x.Phone == id);
            List<SelectListItem> OrderList = new List<SelectListItem>();
            if (supplier != null)
            {
                var order = db.Purchase.Where(x => x.SupplierId == supplier.Id).ToList();
                foreach (var item in order)
                {
                    OrderList.Add(new SelectListItem { Text = item.Id.ToString(), Value = item.Id.ToString() });
                }
            }

            return Json(OrderList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GraphData()
        {
            var date = DateTime.Now;
            var salesData = db.Sales.Where(x => x.SalesDate.Year == date.Year).ToList();
            List<string[]> orderObj = new List<string[]>();
            foreach(var item in salesData.GroupBy(g=>g.SalesDate))
            {
                var res= "gd(" + item.Key.Year + "," + item.Key.Month + "," + item.Key.Day + ")," + item.Select(s => s.TotalAmount).Sum();
                orderObj.Add(new string[] { res});
            }



            //var data = db.Sales
            //    .Where(x => x.SalesDate.Year == date.Year).
            //    GroupBy(g => g.SalesDate.Month)
            //    .Select(s => new { Month = s.Key, No = s.Count() }).ToList();

            //var ret = new[]
            //{
            //new { label="Sales", data = data.Select(x=>new int[]{ x.Month, x.No }),color="#000"},

            //};

            var ret = new[] { new {orderData=orderObj } };
            return Json(ret, JsonRequestBehavior.AllowGet);
        }
    }
}