﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private ShopDb_Hassanuzzaman_01735807216Entities db = new ShopDb_Hassanuzzaman_01735807216Entities();
        public ActionResult Index()
        {
            var sales = db.Sales.ToList();
            var purchase = db.Purchase.ToList();
            var product = db.Product.ToList();
            ViewBag.Income = sales.Sum(x => x.TotalAmount);
            ViewBag.Order = sales.Count();
            ViewBag.Purchase = purchase.Sum(x => x.TotalAmount);
            ViewBag.Product = product.Count();

            return View();
        }
    }
}
