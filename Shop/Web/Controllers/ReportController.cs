﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class ReportController : Controller
    {
        private ShopDb_Hassanuzzaman_01735807216Entities db = new ShopDb_Hassanuzzaman_01735807216Entities();

        public ActionResult ShowSalesReport()
        {
            try
            {
                var data = db.Sales.Select(s => new
                {
                    Date = s.SalesDate,
                    Name = s.Customer.Name,
                    Phone = s.Customer.Phone,
                    TotalAmount = s.TotalAmount,
                    Discount = s.Discount,
                    PaidAmount = s.PaidAmount
                }).ToList();
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Report"), "ItemReport.rpt"));
                rd.DataSourceConnections.Clear();
                rd.Refresh();
                rd.SetDataSource(data);
                rd.SetParameterValue("ReportName", "Sales Report");
                var stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                var memoryStream = new MemoryStream();
                stream.CopyTo(memoryStream);
                var buffer = memoryStream.ToArray();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-length", buffer.Length.ToString());
                Response.BinaryWrite(buffer);
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }



        public ActionResult ShowPurchaseReport()
        {
            try
            {
                var data = db.Purchase.Select(s => new
                {
                    Date = s.PurchaseDate,
                    Name = s.Supplier.Name,
                    Phone = s.Supplier.Phone,
                    TotalAmount = s.TotalAmount,
                    Discount = s.Discount,
                    PaidAmount = s.PaidAmount
                }).ToList();
                var rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/Report"), "ItemReport.rpt"));
                rd.DataSourceConnections.Clear();
                rd.Refresh();
                rd.SetDataSource(data);
                rd.SetParameterValue("ReportName", "Purchase Report");
                var stream = rd.ExportToStream(ExportFormatType.PortableDocFormat);
                var memoryStream = new MemoryStream();
                stream.CopyTo(memoryStream);
                var buffer = memoryStream.ToArray();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-length", buffer.Length.ToString());
                Response.BinaryWrite(buffer);
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }
    }
}