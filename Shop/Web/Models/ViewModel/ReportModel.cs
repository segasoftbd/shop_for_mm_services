﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models.ViewModel
{
    public class ReportModel
    {
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal PaidAmount { get; set; }
    }
}