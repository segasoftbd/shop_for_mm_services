﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopData.Model
{
    public class SalesItems
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int SalesId { get; set; }
        public virtual Sales Sales { get; set; }
        public virtual Product Product { get; set; }
        public int QTY { get; set; }
        public decimal Price { get; set; }
    }
}
