namespace ShopData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ini1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseItems", "PurchaseId", c => c.Int(nullable: false));
            AddColumn("dbo.SalesItems", "SalesId", c => c.Int(nullable: false));
            CreateIndex("dbo.PurchaseItems", "PurchaseId");
            CreateIndex("dbo.SalesItems", "SalesId");
            AddForeignKey("dbo.PurchaseItems", "PurchaseId", "dbo.Purchase", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SalesItems", "SalesId", "dbo.Sales", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SalesItems", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.PurchaseItems", "PurchaseId", "dbo.Purchase");
            DropIndex("dbo.SalesItems", new[] { "SalesId" });
            DropIndex("dbo.PurchaseItems", new[] { "PurchaseId" });
            DropColumn("dbo.SalesItems", "SalesId");
            DropColumn("dbo.PurchaseItems", "PurchaseId");
        }
    }
}
